#!/usr/bin/env bash

RE='[^0-9]*\([0-9]*\)[.]\([0-9]*\)[.]\([0-9]*\)\([0-9A-Za-z-]*\)'

version_ref="$1"
if [ -z "$1" ];then
  version_ref=v0.0.0
fi

MAJOR=$(echo $version_ref | sed -e "s#$RE#\1#")
MINOR=$(echo $version_ref | sed -e "s#$RE#\2#")

base=$(git --no-pager tag | grep -E "^v${MAJOR}\.${MINOR}\.[0-9]{1,}$" | sort -V | tail -1)
PATCH=`echo $base | sed -e "s#$RE#\3#"`

# If this base 
if [[ -z "$base" ]]; then
  echo "${version_ref}"
  exit
fi

let PATCH+=1

echo "v$MAJOR.$MINOR.$PATCH"

